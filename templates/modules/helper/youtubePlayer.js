const ytdl = require('ytdl-core-discord');
async function play(connection, url) {
   const dispatcher =connection.playOpusStream(
   await ytdl(url, { // provide ytdl library with the song url
      volume: 0.1, 
      quality: 'highestaudio', // highest audio quality
      highWaterMark: 1024 * 1024 * 10 // this line downloads part of the song before starting, it reduces stuttering
    })
    )
   dispatcher.on('end', (reason) => {
    // The song has finished
    console.log(reason + ', disconnecting');
    connection.disconnect()
  });
  
  dispatcher.on('error', e => {
    // Catch any errors that may arise
    console.log(e);
    connection.disconnect()
  });
  }
module.exports.play = play;