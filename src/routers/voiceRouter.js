const events = require('events')
fs = require("fs");
const emitter = new events.EventEmitter();



/**
 * Handles the first line of routing. It checks if the message was for the bot and if it was not sent by itself.
 * 
 * @param {GuildMember} user 
 * an instance of GuildMember as defined in https://discord.js.org/#/docs/main/stable/class/GuildMember
 * this is the user doing an action in the voicechannel
 * 
 * @param {State} state
 * a state from the enum to define the action
 * 
 * 
 * 
 * @returns void
 */
const process = function (user, state) 
{
    emitter.emit(state, user);
};


module.exports = {

    subscribe: function (state, callback) 
    {
        emitter.addListener(state, callback);
    },
    handle: function (newMember, oldMember)
    {
        let newUserChannel = newMember.voiceChannel;
        let oldUserChannel = oldMember.voiceChannel;

        
        if (oldUserChannel === undefined && newUserChannel !== undefined) {

            process(newMember, Constants.voiceState.ENTERING);

        } else if (newUserChannel === undefined) {
             process(oldMember, Constants.voiceState.LEAVING);
        } else if (oldUserChannel !== undefined && newUserChannel !== undefined) {
            process(oldMember, Constants.voiceState.LEAVING);
            process(newMember, Constants.voiceState.ENTERING);
        } else {
            console.error('Voice chat movement not yet supported');
        }
    }
};

// making all voice modules subscribe

[Constants.voiceCustomDir, Constants.voiceDir].forEach((path)=>
    {   
        fs.readdirSync( path ).forEach(function(file) 
        {
            if(!(file[0] === '.') )
            {
                require(path + '/' + file);
                console.log('required ' + path + '/' + file )
            }
        }
    );
    
    }
)

