const events = require('events');
fs = require("fs");
const emitter = new events.EventEmitter();

module.exports = {

    handle: function (message) {
        const client = message.client;
        const text = message.toString();
        // These functions will be called on a message where the bot is tagged 
        if (message.author.id !== client.user.id && message.isMentioned(client.user.id)) 
        {
                emitter.emit('message',message);
        }
    },
    subscribe: function(callback){
        emitter.addListener("message", callback);
    }
};


// making all message modules subscribe

[Constants.messageCustomDir, Constants.messageDir].forEach((path)=>
    {   
        fs.readdirSync( path ).forEach(function(file) 
        {
            if(!(file[0] === '.') )
            {
                require(path + '/' + file);
                console.log('required ' + path + '/' + file )
            }
        }
    );
    
    }
)
