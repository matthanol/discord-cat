const optionsRepo = require('./optionsRepository')
//global.__basedir ="./src/" ;

test( 'options can be read', ()=>{
    return optionsRepo.getOptionsJson().then(data => {
        expect(typeof(data)).toBe("object")
    })
})

test('token is present in options', () => {
    return optionsRepo.getOptionsJson().then(data => {
        expect(typeof(data.token)).toBe("string")
    })
});