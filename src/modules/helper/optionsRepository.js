const fs = require('fs')


module.exports ={
 getToken: function() {
   return new Promise((res,rej)=>{
       this.getOptionsJson().then( (data)=>{
           const token = data.token;
           if (token === ""){
               rej('Empty token')
           }else(
                res(token)
           )
           
       }, (reason)=>{
           rej(reason)
       }
       )
   })

},
getOptionsJson:function (){
    return new Promise((res,rej)=>{
        console.log(Constants.baseDir)
        fs.readFile(Constants.baseDir +'/options.json', (err, data)=>{
            if(err){
                rej(err)
            }else{
                res(JSON.parse(data))
            }
        })
    })
},

};

