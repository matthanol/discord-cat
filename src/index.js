global.Constants = require('./Constants');
const Discord = require('discord.js');
const OptionsRepo = require('./modules/helper/optionsRepository');
const client = new Discord.Client();
const messageRouter = require('./routers/messageRouter');
const voiceRouter = require('./routers/voiceRouter');
client.on('ready', () => {
    console.log('logged in as ' + client.user.tag);
});

// handles message input
client.on('message', msg => messageRouter.handle(msg));
client.on('voiceStateUpdate', (oldMember, newMember) => {
    voiceRouter.handle(oldMember, newMember);
})

//logging in
OptionsRepo.getToken().then((token) => {
        client.login(token);
    },
    (reason) => {
        console.error(reason);
        process.exit(1);
    })

