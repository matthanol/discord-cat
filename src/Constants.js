let rootPath = require('app-root-path').toString()
module.exports = {
    baseDir : rootPath ,
    messageDir : "modules/message",
    voiceDir : "modules/voice",
    helperDir : "modules/helper",
    messageCustomDir : rootPath + "/" + "modules/message",
    voiceCustomDir : rootPath + "/" + "modules/voice",
    voiceState : 
    {
        NONE: 0,
        ENTERING: 1,
        LEAVING: 2,
        MUTING: 3,
        DEAFENING: 4,
        UNMUTED: 5,
        UNDEAFENED: 6,
    },

}
